import React from 'react';
import { AppRegistry, YellowBox } from 'react-native';
import { Provider } from 'react-redux';
import store from './app/redux/store';
import App from './app/App';
import { name as appName } from './app.json';

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Class RCTCxxModule', 'Module RCTImageLoader', 'Remote debugger', 'Required dispatch_sync', 'You should only render']);

// inicializace + spojeni apliakce s reduxem
const Root = () => {
    return (
        <Provider store={store}>
            <App />
        </Provider>
    );
};

AppRegistry.registerComponent(appName, () => Root);
