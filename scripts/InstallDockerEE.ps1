# Script for installation of Docker for Windows Server 2016, setting custom remote API port of Docker and allowing it in firewall
#needed to executed as Administrator

#installation of module for Docker
Install-Module -Name DockerMsftProveder -Force
#installation of Docker package
Install-Package -Name docker -ProviderName DockerMsftProvider -Force

#automatic start of Docker service after boot
Set-Service -Name docker -StartupType Automatic

#config of custom remote api port
echo '{"hosts":["tcp://0.0.0.0:8593"]}' > "C:\ProgramData\docker\config\daemon.json"

#added firewall rule for Docker Remote API operations or second option is turn firewall off
New-NetFirewallRule -DisplayName 'SSH Inbound' -Profile @('Domain','Private','Public') -Direction Inbound -Action Allow -Protocol TCP -LocalPort @('8593')

#run Docker service
Start-Service -Name docker