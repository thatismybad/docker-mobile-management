#!/bin/bash
# Script for installation of Docker for Ubuntu 16.04 LTS, setting custom remote API port of Docker and disable firewall
#needed to by executed as root

#update operating system
apt-get update && apt-get -y upgrade

#adding Docker’s official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
#adding Docker stable repository
sudo add-apt-repository \
   "deb [arch=armhf] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
#update of packages from Docker repository
apt-get update
#install of Docker CE - latest version
apt-get install docker-ce

#Firewall disable
ufw disable

#stopping of Docker service (for sure)
service docker stop
#setting of custom remote API port
sed 's/ExecStart=/usr/bin/dockerd -H fd:// /ExecStart=/usr/bin/dockerd -H fd:// -H tcp://0.0.0.0:8593/' /lib/systemd/system/docker.service
#reload of daemon
systemctl daemon-reload
#start of Docker service
service docker start
