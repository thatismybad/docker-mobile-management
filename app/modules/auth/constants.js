/**
 * Trida s konstantami pro auth modul
 */
export const AUTH_SET_NET_CONN = 'AUTH_SET_NET_CONN';
export const AUTH_SIGNIN_USER = 'AUTH_SIGNIN_USER';
export const AUTH_SIGNIN_USER_SUCCESS = 'AUTH_SIGNIN_USER_SUCCESS';
export const AUTH_SIGNIN_USER_FAIL = 'AUTH_SIGNIN_USER_FAIL';
