import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux'

import { ASYNC_ST_USER_KEY, ASYNC_ST_TOKEN_KEY, ASYNC_ST_UQ_TOKEN_KEY } from '../../../../utils/constants';

import { getStorageItem, getStorageItems } from '../../../../utils/authUtils';
import { buildToken } from '../../../../utils/tokenUtils';
import { setRequestToken } from '../../../main/reducers/sharedReducer';

/**
 * Komponenta predstavujici inicializacni obrazovku aplikace;
 * stara se o overeni jestli uzivatel je prihlasen v aplikaci nebo ne ->
 * podle vysledku zobrazi prihlaseni do aplikace nebo autorizovanou cast
 */
 class Index extends React.Component {
    constructor(props) {
        super(props);
        this.auth();
    }

    // overovaci metoda, ktera vyuzije utilizacni metody v authUtils pro ziskani uzivatele v lokalnim ulozisti
    async auth() {
        const { navigation } = this.props;
        await getStorageItem(ASYNC_ST_USER_KEY)
            .then((user) => {
                if (user !== null && user !== undefined) {
                    getStorageItems([ASYNC_ST_TOKEN_KEY, ASYNC_ST_UQ_TOKEN_KEY])
                    .then((tokens) => {
                        const requestToken = buildToken(tokens[0], tokens[1]);
                        this.props.setRequestToken(requestToken)
                        navigation.navigate('App');
                    })
                } else {
                    navigation.navigate('Auth');
                }
            });
    }

    render() {
        return (
            <View>
                <ActivityIndicator />
            </View>
        );
    }
}

// spojeni komponenty s reduxem
export default connect(null, { setRequestToken })(Index);

