import React, { Component } from 'react';
import {
    Alert, ActivityIndicator, Text,
    TouchableOpacity, Image, TextInput,
    View, KeyboardAvoidingView,
} from 'react-native';
import { connect } from 'react-redux';

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import bgFile from '../../../../assets/login_screen.jpg';
import styles from './styles';
import * as s from '../../../../utils/strings';
import * as gC from '../../../../utils/constants';
import { colorWhite, lightGrey140 } from '../../../../utils/colors';

import { getStorageItems } from '../../../../utils/authUtils';
import { signIn } from '../../reducer';
import { setRequestToken } from '../../../main/reducers/sharedReducer';

/**
 * Komponenta predstavujici prihlasovaci obrazovku
 */
class SignIn extends Component {
    static navigationOptions = {
        headerTransparent: true,
    };

    // interni stav komponenty s predvyplnenymi udaji
    constructor(props) {
        super(props);
        this.state = {
            username: 'duckling',
            password: '1am.potato',
            loginPressed: false,
        };
    }

    // nacteni dat z navigace a pocatecniho stavu reduxu pro auth modul
    componentDidUpdate() {
        const { navigate } = this.props.navigation;
        const {
            isSuccess,
            loading,
            code,
        } = this.props.auth;

        const { loginPressed } = this.state;
        // obsluha prihlaseni
        if (loginPressed) {
            if (!loading) {
                if (isSuccess) {
                    navigate('App');
                    this.setToken();
                    this.resetLoginForm();
                } else {
                    let errorTitle;
                    let errorMsg;
                    switch (code) {
                    case 401:
                        errorTitle = s.ERROR_SIGN_IN_FAILED_TITLE;
                        errorMsg = s.ERROR_SIGN_IN_FAILED_TEXT;
                        break;
                    default:
                        errorTitle = s.ERROR_UNKNOWN_ERROR_TITLE;
                        errorMsg = s.ERROR_UNKNOWN_ERROR_TEXT;
                        break;
                    }
                    this.displayErrorDialog(errorTitle, errorMsg);
                }
            }
        }
    }

    // funkce pro prihlaseni uzivatele
    onLoginPressed() {
        const { username, password } = this.state;
        this.setState({ loginPressed: true });
        this.props.signIn(username, password);
    }

    async setToken() {
        await getStorageItems([gC.ASYNC_ST_TOKEN_KEY, gC.ASYNC_ST_UQ_TOKEN_KEY])
            .then((tokens) => {
                const requestToken = buildToken(tokens[0], tokens[1]);
                this.props.setRequestToken(requestToken)
        });
    }

    // resetovani prihlasovaciho formulare po uspesnem prihlaseni
    resetLoginForm() {
        this.setState({
            username: '',
            password: '',
            loginPressed: false,
        });
    }

    // obecna metoda pro zobrazeni chyboveho dialogu
    displayErrorDialog(title, message) {
        Alert.alert(
            title,
            message,
            [
                { text: 'OK', style: 'cancel' },
            ],
            { cancelable: false },
        );
        this.setState({ loginPressed: false });
    }

    // vykreslovaci metoda celeho UI teto obrazovky (s kazdou aktualizaci stavu se prekresluje)
    render() {
        const { username, password, loginPressed } = this.state;
        return (
            <KeyboardAvoidingView
                style={styles.container}
                behavior="padding"
            >
                <Image source={bgFile} style={styles.backgroundImage} />
                <View style={styles.innerContainer}>
                    <View style={styles.logo}>
                        <FontAwesome5 name="docker" color={colorWhite} size={200} />
                    </View>
                    <View style={styles.loginForm}>
                        <View style={styles.inputWrapper}>
                            <TextInput
                                style={styles.input}
                                onChangeText={newUsername => this.setState({ username: newUsername })}
                                value={username}
                                placeholder={s.LOGIN_USERNAME_HINT}
                                placeholderTextColor={lightGrey140}
                                underlineColorAndroid="transparent"
                                autoCapitalize="none"
                            />
                        </View>
                        <View style={styles.inputWrapper}>
                            <TextInput
                                style={styles.input}
                                onChangeText={newPassword => this.setState({ password: newPassword })}
                                value={password}
                                placeholder={s.LOGIN_PASSWORD_HINT}
                                placeholderTextColor={lightGrey140}
                                underlineColorAndroid="transparent"
                                secureTextEntry
                            />
                        </View>
                        <TouchableOpacity
                            style={styles.button}
                            disabled={loginPressed}
                            onPress={() => this.onLoginPressed()}
                        >
                            {
                                loginPressed
                                    ? <ActivityIndicator />
                                    : (
                                        <Text style={styles.buttonText}>
                                            {s.LOGIN_SIGNIN_BUTTON.toUpperCase()}
                                        </Text>
                                    )
                            }
                        </TouchableOpacity>
                    </View>
                </View>
                <Image />
            </KeyboardAvoidingView>
        );
    }
}

// namapovani stavu reduceru z auth modulu na lokalni property
const mapStateToProps = (state) => {
    return {
        auth: state.authReducer,
        shared: state.sharedReducer
    };
};

// spojeni komponenty s reduxem
export default connect(mapStateToProps, { signIn, setRequestToken })(SignIn);
