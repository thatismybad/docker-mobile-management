import { StyleSheet } from 'react-native';
import * as c from '../../../../utils/colors';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    innerContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
        position: 'absolute',
    },
    logo: {
        flex: 1,
        backgroundColor: c.colorTransparent,
        top: '10%',
        alignItems: 'center',
    },
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'cover', // or 'stretch',
    },
    loginForm: {
        flex: 1,
        width: '60%',
        backgroundColor: c.colorTransparent,
        alignItems: 'center',
        position: 'absolute',
        top: '55%',
        left: '20%',
        right: '20%',
        bottom: '5%',
    },
    inputWrapper: {
        width: '100%',
        backgroundColor: c.colorBlackTransparent,
        borderRadius: 25,
        marginBottom: 5,
        borderWidth: 1,
        borderColor: c.colorWhite,
    },
    input: {
        height: 50,
        color: c.colorWhite,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    button: {
        width: '100%',
        marginTop: 10,
        paddingTop: 15,
        paddingBottom: 15,
        marginLeft: 30,
        marginRight: 30,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: c.colorWhite,
        backgroundColor: c.colorWhite,
        alignItems: 'center',
    },
    buttonText: {
        color: c.colorBlack,
    },
});

export default styles;
