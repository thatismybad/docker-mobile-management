/**
 * Trida obsluhujici redux cast auth modulu - styl podle Ducks vzoru
 */
import * as c from './constants';
import * as gC from '../../utils/constants';

import { setStorageItem, getStorageItem } from '../../utils/authUtils';
import { getToken } from '../../utils/tokenUtils';

// pocatecni stav
const initialState = {
    message: '',
    loading: false,
};

// akce pro prihlaseni
export const signIn = (username, password) => {
    return {
        type: c.AUTH_SIGNIN_USER,
        user: username,
        payload: {
            request: {
                url: '/user/signin',
                method: 'post',
                data: {
                    username,
                    password,
                },
            },
        },
    };
};

// reducer - zpracovani akci a jejich vysledku
const reducer = (state = initialState, action) => {
    switch (action.type) {
    case c.AUTH_SIGNIN_USER:
        setStorageItem(gC.ASYNC_ST_USER_KEY, action.user);
        return {
            ...state,
            loading: true,
        };
    case c.AUTH_SIGNIN_USER_SUCCESS: {
        const { token } = action.payload.data;
        setStorageItem(gC.ASYNC_ST_TOKEN_KEY, getToken(token, gC.JWT_TOKEN));
        setStorageItem(gC.ASYNC_ST_UQ_TOKEN_KEY, getToken(token, gC.UQ_TOKEN));
        return {
            ...state,
            isSuccess: true,
            message: '',
            loading: false,
        };
    }
    case c.AUTH_SIGNIN_USER_FAIL: {
        const { message, code } = action.error.response.data.meta;
        return {
            ...state,
            isSuccess: false,
            message,
            code,
            loading: false,
        };
    }
    default:
        return state;
    }
};

export default reducer;
