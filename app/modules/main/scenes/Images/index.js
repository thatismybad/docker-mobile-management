import React, { Component } from 'react';
import {
    View, FlatList, TouchableOpacity,
    Platform,
} from 'react-native';
import { connect } from 'react-redux';

import styles from './styles';

import ImageListItem from '../../components/Images/ImageListItem';
import ServerPicker from '../../components/ServerPicker';
import FAButton from '../../components/FAButton';
import { IMAGE_ADD_ROUTE, IMAGE_DETAIL_ROUTE } from '../../../../utils/constants';

// navigacni FAB tlacitko pro Android OS
const ActionButton = ({ nav }) => {
    return (
    <TouchableOpacity 
        onPress={() => nav.navigate(IMAGE_ADD_ROUTE)}
    >
        <FAButton />
    </TouchableOpacity>
)}

// obrazovka se seznamem obrazu na vybranem prostredi
class Images extends Component {

    // metoda pro renderovani jednotlivych prvku seznamu s proklikem do detailu
    renderItem = ({ item }) => (
        <TouchableOpacity
            onPress={() => this.props.screenProps.navigation.navigate(IMAGE_DETAIL_ROUTE, { id: item.Id })}
        >
            <ImageListItem image={item} />
        </TouchableOpacity>
    );

    // vyrenderovani oddelovace prvku seznamu
    renderSeparator = () => <View style={styles.divider} />

    render() {
        const { images } = this.props.image;

        return (
            <View style={styles.container}>
                <ServerPicker screen="imag" />
                <FlatList
                    styles={styles.list}
                    data={images}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => `${index}`}
                    ItemSeparatorComponent={this.renderSeparator}
                />
                {
                    Platform.OS === 'android' ? <ActionButton nav={this.props.screenProps.navigation} /> : ''
                }
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        image: state.imageReducer,
        shared: state.sharedReducer,
    };
};

export default connect(mapStateToProps)(Images);
