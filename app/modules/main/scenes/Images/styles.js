import { StyleSheet } from 'react-native';
import * as c from '../../../../utils/colors';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    title: {
        color: c.darkerGrey60,
        fontSize: 18,
        margin: 10,
    },
    list: {
        flex: 1,
        margin: 10,
    },
    item: {
        height: 40,
        borderColor: c.lightGrey120,
        paddingLeft: 10,
        paddingRight: 10,
        justifyContent: 'center',
    },
    itemContent: {
        paddingTop: 5,
        paddingBottom: 5,
        flexDirection: 'row',
        alignContent: 'center',
    },
    itemText: {
        paddingLeft: 15,
        fontSize: 15,
    },
    divider: {
        height: 1,
        width: '100%',
        backgroundColor: c.lightGrey130,
    },
});

export default styles;
