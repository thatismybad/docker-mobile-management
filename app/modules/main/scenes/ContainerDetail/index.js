import React, { Component } from 'react';
import {
    View, Text, Button, Alert,
    ActivityIndicator,
} from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation'; 

import styles from './styles';
import gStyles from '../../../../styles/theme';
import * as s from '../../../../utils/strings';

import AppStatusBar from '../../../../components/AppStatusBar';

import { getContainerDetail, getContainers, removeContainer, startContainer, stopContainer } from '../../reducers/containerReducer';
import BackNav from '../../../../components/BackNav';

class ContainerDetail extends Component {
    componentDidMount() {
        const idParam = this.props.navigation.getParam('id', '-');
        const { requestToken, selectedServer } = this.props.shared;
        this.props.getContainerDetail(requestToken, selectedServer, idParam);
    }

    refresh() {
        const { requestToken, selectedServer } = this.props.shared;
        const { id } = this.props.container.containerDetail;
        this.props.getContainerDetail(requestToken, selectedServer, id);
    }

    startContainer() {
        const { requestToken, selectedServer } = this.props.shared;
        const { id } = this.props.container.containerDetail;
        this.props.startContainer(requestToken, selectedServer, id);
        this.refresh();
    }

    stopContainer() {
        const { requestToken, selectedServer } = this.props.shared;
        const { id } = this.props.container.containerDetail;
        this.props.stopContainer(requestToken, selectedServer, id)
        this.refresh();
    }

    removeContainer() {
        const { requestToken, selectedServer } = this.props.shared;
        const { id } = this.props.container.containerDetail;
        return Alert.alert(
            s.DELETE_CONTAINER_TITLE,
            s.EMPTY_MSG,
            [
                {
                    text: s.BTN_OK,
                    onPress: () => {
                        this.props.removeContainer(requestToken, selectedServer, id);
                        this.props.getContainers(requestToken, selectedServer);
                        this.props.navigation.dispatch(NavigationActions.back());
                    },
                },
                { text: s.BTN_CANCEL },
            ],
        );
    }

    render() {
        const { loading, containerDetail } = this.props.container;
        if (loading) {
            return <ActivityIndicator style={gStyles.loading} />;
        }

        const { labels, volumes, ports } = containerDetail;
        const labelArray = [];
        const volumeArray = [];
        const portArray = [];
        labelArray.push(labels);
        volumeArray.push(volumes);
        portArray.push(ports);

        return (
            <View>
                <AppStatusBar />
                <BackNav nav={this.props.navigation} title={containerDetail.name} />
                <View style={styles.container}>
                    <Text style={styles.label}>Název: </Text>
                    <Text style={styles.text}>{containerDetail.name}</Text>
                </View>
                <View style={styles.container}>
                    <Text style={styles.label}>ID: </Text>
                    <Text style={styles.text}>{containerDetail.id}</Text>
                </View>
                <View style={styles.container}>
                    <Text style={styles.label}>Rodičovský obraz: </Text>
                    <Text style={styles.text}>{containerDetail.parentImage}</Text>
                </View>
                <View style={styles.container}>
                    <Text style={styles.label}>Stav: </Text>
                    <Text style={styles.text}>{containerDetail.status}</Text>
                </View>
                <View style={styles.container}>
                    <Text style={styles.label}>Hostname: </Text>
                    <Text style={styles.text}>{containerDetail.hostname}</Text>
                </View>
                <View style={styles.container}>
                    <Text style={styles.label}>Značky: </Text>
                    <Text style={styles.text}>{
                        labelArray.map((label, key) =><Text key={key}>{JSON.stringify(label)}</Text>)
                        }
                    </Text>
                </View>
                <View style={styles.container}>
                    <Text style={styles.label}>Porty: </Text>
                    <Text style={styles.text}>{JSON.stringify(containerDetail.ports)}</Text>
                </View>
                <View>
                    <Button onPress={() => this.startContainer()} title="Spustit kontejner" />
                    <Button onPress={() => this.stopContainer()} title="Zastavit kontejner" />
                    <Button onPress={() => this.removeContainer()} title="Smazat kontejner" />
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        container: state.containerReducer,
        shared: state.sharedReducer,
    };
};

export default connect(mapStateToProps, { getContainerDetail, getContainers, removeContainer, startContainer, stopContainer })(ContainerDetail);
