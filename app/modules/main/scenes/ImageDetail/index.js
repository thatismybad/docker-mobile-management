import React, { Component } from 'react';
import {
    View, Text, Button, Alert,
    ActivityIndicator,
} from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';

import styles from './styles';
import gStyles from '../../../../styles/theme';
import * as s from '../../../../utils/strings'

import AppStatusBar from '../../../../components/AppStatusBar';

import { getImageDetail, removeImage, getImages } from '../../reducers/imageReducer';
import BackNav from '../../../../components/BackNav';

class ImageDetail extends Component {
    componentDidMount() {
        console.log(this.props)
        const idParam = this.props.navigation.getParam('id', '-');
        const { requestToken, selectedServer } = this.props.shared;
        this.props.getImageDetail(requestToken, selectedServer, idParam);
    }

    removeImage(id) {
        const { requestToken, selectedServer } = this.props.shared;
        return Alert.alert(
            s.DELETE_IMAGE_TITLE,
            s.EMPTY_MSG,
            [
                {
                    text: s.BTN_OK,
                    onPress: () => {
                        this.props.removeImage(requestToken, selectedServer, id);
                        this.props.getImages(requestToken, selectedServer);
                        this.props.navigation.dispatch(NavigationActions.back());
                    },
                },
                { text: s.BTN_CANCEL },
            ],
        );
    }

    render() {
        const { loading, imageDetail } = this.props.image;
        if (loading) {
            return <ActivityIndicator style={gStyles.loading} />;
        }
        return (
            <View>
                <AppStatusBar />
                <BackNav nav={this.props.navigation} title={imageDetail.name} />
                <View style={styles.container}>
                    <Text style={styles.label}>Název: </Text>
                    <Text style={styles.text}>{imageDetail.name}</Text>
                </View>
                <View style={styles.container}>
                    <Text style={styles.label}>ID: </Text>
                    <Text style={styles.text}>{imageDetail.id}</Text>
                </View>
                <View style={styles.container}>
                    <Text style={styles.label}>Architektura: </Text>
                    <Text style={styles.text}>{imageDetail.arch}</Text>
                </View>
                <View style={styles.container}>
                    <Text style={styles.label}>OS: </Text>
                    <Text style={styles.text}>{imageDetail.os}</Text>
                </View>
                <View style={styles.container}>
                    <Text style={styles.label}>Velikost: </Text>
                    <Text style={styles.text}>{imageDetail.size}</Text>
                </View>
                <Button onPress={() => this.removeImage(imageDetail.id)} title="Smazat obraz" />
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        image: state.imageReducer,
        shared: state.sharedReducer,
    };
};

export default connect(mapStateToProps, { getImageDetail, removeImage, getImages })(ImageDetail);
