import React, { Component } from 'react';
import {
    View, FlatList, TouchableOpacity,
    Platform,
} from 'react-native';
import { connect } from 'react-redux';

import styles from './styles';

import { CONTAINER_DETAIL_ROUTE } from '../../../../utils/constants';

import ContainerListItem from '../../components/Containers/ContainerListItem';
import ServerPicker from '../../components/ServerPicker';
import FAButton from '../../components/FAButton';

// obrazovka se seznamem kontejneru vybraneho serveru
class Containers extends Component {
    renderItem = ({ item }) => (
        <TouchableOpacity
            onPress={() => this.props.screenProps.navigation.navigate(CONTAINER_DETAIL_ROUTE, { id: item.Id })}
        >
            <ContainerListItem container={item} />
        </TouchableOpacity>
    );

    renderSeparator = () => <View style={styles.divider} />

    render() {
        const { containers } = this.props.container;

        return (
            <View style={styles.container}>
                <ServerPicker screen="cont" />
                <FlatList
                    styles={styles.list}
                    data={containers}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => `${index}`}
                    ItemSeparatorComponent={this.renderSeparator}
                />
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        container: state.containerReducer,
        shared: state.sharedReducer,
    };
};

export default connect(mapStateToProps)(Containers);
