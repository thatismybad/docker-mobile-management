import React, { Component } from 'react';
import {
    View, Text, Button, TouchableOpacity,
    ActivityIndicator,
} from 'react-native';
import { connect } from 'react-redux';

import gStyles from '../../../../styles/theme';
import styles from './styles';

import { getEnvInfo } from '../../reducers/dashboardReducer';
import AppStatusBar from '../../../../components/AppStatusBar';
import BackNav from '../../../../components/BackNav';

// obrazovka s detailnimi informacemi o vybranem prostredi
class EnvDetail extends Component {
    componentDidMount() {
        const envParam = this.props.navigation.getParam('env', '-');
        const { requestToken } = this.props.shared;
        this.props.getEnvInfo(requestToken, envParam);
    }

    render() {
        const { loading, envDetail } = this.props.dashboard;
        console.log(envDetail)
        if (loading) {
            return <ActivityIndicator style={gStyles.loading} />;
        }

        return (
            <View>
                <AppStatusBar />
                <BackNav nav={this.props.navigation} title={envDetail.env}/>
                <View style={styles.container}>
                    <Text style={styles.label}>Prostředí: </Text>
                    <Text style={styles.text}>{envDetail.env}</Text>
                </View>
                <View style={styles.container}>
                    <Text style={styles.label}>Název: </Text>
                    <Text style={styles.text}>{envDetail.name}</Text>
                </View>
                <View style={styles.container}>
                    <Text style={styles.label}>URL: </Text>
                    <Text style={styles.text}>{envDetail.url}</Text>
                </View>
                <View style={styles.container}>
                    <Text style={styles.label}>Port: </Text>
                    <Text style={styles.text}>{envDetail.port}</Text>
                </View>
                <View style={styles.container}>
                    <Text style={styles.label}>Hostname: </Text>
                    <Text style={styles.text}>{envDetail.hostname}</Text>
                </View>
                <View style={styles.container}>
                    <Text style={styles.label}>OS: </Text>
                    <Text style={styles.text}>{envDetail.os}</Text>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        dashboard: state.dashboardReducer,
        shared: state.sharedReducer,
    };
};

export default connect(mapStateToProps, { getEnvInfo })(EnvDetail);
