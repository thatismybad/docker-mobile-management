import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        marginLeft: 10,
    },
    label: {
        fontSize: 15,
        fontWeight: 'bold',
    },
    text: {
        fontSize: 15,
        marginLeft: 10
    }
});

export default styles;
