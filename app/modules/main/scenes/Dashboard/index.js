import React, { Component } from 'react';
import {
    View, ActivityIndicator, 
} from 'react-native';
import { connect } from 'react-redux';

import EnvInfo from '../../components/Dashboard/EnvInfo';
import ServerInfo from '../../components/Dashboard/ServerInfo';

import { getAllEnvInfo, getServerInfo, turnOffEnvLoaded } from '../../reducers/dashboardReducer';
import { setSelectedServer } from '../../reducers/sharedReducer';

import gStyles from '../../../../styles/theme';

import styles from './styles';

// obrazovka s prehledem prostredi a serveru
class Dashboard extends Component {
    async componentDidMount() {
        const { requestToken } = this.props.shared;
        this.props.getAllEnvInfo(requestToken);
    }

    render() {
        const { envInfo, loading } = this.props.dashboard;
        if (loading) return <ActivityIndicator style={gStyles.loading} />;
        return (
            <View style={styles.container}>
                <EnvInfo envInfo={envInfo} nav={this.props.screenProps.navigation}/>
                <ServerInfo requestToken={this.props.shared.requestToken} />
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        dashboard: state.dashboardReducer,
        shared: state.sharedReducer,
    };
};

export default connect(mapStateToProps,
    {
        getAllEnvInfo,
        getServerInfo,
        turnOffEnvLoaded,
        setSelectedServer,
    })(Dashboard);
