import { StyleSheet } from 'react-native';
import * as c from '../../../../utils/colors';

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
    },
    search: {
        flexDirection: 'row',
        marginLeft: 10,
        marginRight: 10,
        alignItems: 'center',
    },
    button: {
        width: 40,
        height: 40,
        marginLeft: 10,
        marginBottom: 10,
        padding: 5,
        borderColor: c.darkerGrey120,
        borderWidth: 1,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    input: {
        flex: 1,
        height: 40,
        fontSize: 15,
        padding: 5,
        borderColor: c.darkerGrey120,
        borderWidth: 1,
        borderRadius: 5,
        marginBottom: 10,
    },
    list: {
        margin: 10,
    },
    item: {
        height: 40,
        borderColor: c.lightGrey120,
        paddingLeft: 10,
        paddingRight: 10,
        justifyContent: 'center',
    },
    itemContent: {
        paddingTop: 5,
        paddingBottom: 5,
        flexDirection: 'row',
        alignContent: 'center',
    },
    itemText: {
        paddingLeft: 15,
        fontSize: 15,
    },
    divider: {
        height: 1,
        width: '100%',
        backgroundColor: c.lightGrey130,
    },
});

export default styles;
