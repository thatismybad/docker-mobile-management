import React, { Component } from 'react';
import {
    View, FlatList, Alert,
    TextInput, TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';

import Feather from 'react-native-vector-icons/Feather'; 

import styles from './styles';
import gStyles from '../../../../styles/theme';
import * as s from '../../../../utils/strings'

import AppStatusBar from '../../../../components/AppStatusBar';

import { searchForImage, getImages, pullImage } from '../../reducers/imageReducer';
import BackNav from '../../../../components/BackNav';
import ServerPicker from '../../components/ServerPicker';
import ImageSearchItem from '../../components/Images/ImageSearchItem';

// obrazovka pro vyhledani a stazeni Docker obrazu z Docker Hub repository
class ImageAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchString: '',
        }
    }

    // dialog pro potvrzeni stazeni vybraneho obrazu
    pullImage(image) {
        const { requestToken, selectedServer } = this.props.shared;
        return Alert.alert(
            s.PULL_IMAGE_TITLE,
            `${s.PULL_IMAGE_MSG} ${image}`,
            [
                {
                    text: s.BTN_OK,
                    onPress: () => {
                        this.props.pullImage(requestToken, selectedServer, image);
                        this.props.getImages(requestToken, selectedServer);
                        this.props.navigation.dispatch(NavigationActions.back());
                    },
                },
                { text: s.BTN_CANCEL },
            ],
        );
    }

    // render metoda pro jednotlive polozky seznamu
    renderItem = ({ item }) => (
        <TouchableOpacity
            onPress={() => this.pullImage(item.name)}
        >
            <ImageSearchItem image={item} />
        </TouchableOpacity>
    );

    // vyrenderovani oddelovace mezi prvky seznamu
    renderSeparator = () => <View style={styles.divider} />

    render() {
        const { searchResults } = this.props.image;
        const { requestToken, selectedServer } = this.props.shared;

        return (
            <View>
                <AppStatusBar />
                <BackNav nav={this.props.navigation} title="Stažení obrazu" />
                <ServerPicker screen="imag_add" />
                <View style={styles.container}>
                    <View style={styles.search}>
                        <TextInput
                            style={styles.input}
                            placeholder="Vyhledat obraz..."
                            onChangeText={(text) => this.setState({ searchString: text })}
                            value={this.state.searchString}
                            />
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => this.props.searchForImage(requestToken, selectedServer, this.state.searchString)}>
                            <Feather name="search" size={30} color="black" />
                        </TouchableOpacity>
                    </View>
                    <FlatList
                        styles={styles.list}
                        data={searchResults}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => `${index}`}
                        ItemSeparatorComponent={this.renderSeparator}
                    />
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        image: state.imageReducer,
        shared: state.sharedReducer,
    };
};

export default connect(mapStateToProps, { searchForImage, getImages, pullImage })(ImageAdd);
