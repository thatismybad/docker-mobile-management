import React, { Component } from 'react';
import {
    View, Text, ScrollView
} from 'react-native';
import { connect } from 'react-redux';

import { setSelectedServer } from '../../reducers/sharedReducer';
import { getNodesInfo } from '../../reducers/swarmReducer';

import ServerPicker from '../../components/ServerPicker';
import NodeItem from '../../components/Swarm/NodeItem';

import styles from './styles';

// obrazovka s prehledem swarmu
class Swarm extends Component {
    render() {
        const { nodes, error } = this.props.swarm;
        return (
            <View style={styles.container}>
                <ServerPicker screen="swrm" />
                <ScrollView>
                    {
                        error
                        ? 
                            <View style={styles.center}><Text>{error}</Text></View>
                        :
                            nodes.map((node, key) => {
                                return <NodeItem key={key} node={node} />
                            })
                    }
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        swarm: state.swarmReducer,
        shared: state.sharedReducer,
    };
};

export default connect(mapStateToProps,
    {
        setSelectedServer,
        getNodesInfo
    })(Swarm);
