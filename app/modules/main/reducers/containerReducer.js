/**
 * Trida obsluhujici redux cast pro kontejnery main modulu - styl podle Ducks vzoru
 */
import * as c from '../constants';
import storeContainerInfo from '../utils/containerHandler';

// pocatecni stav
const initialState = {
    loading: false,
    containers: [],
    containerDetail: {}
};

// akce pro ziskani seznamu kontejneru vybraneho serveru
export const getContainers = (token, env) => {
    return {
        type: c.GET_CONTAINERS,
        payload: {
            request: {
                url: `/api/${env}/containers/json?all=1`,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        },
    };
};

// akce pro ziskani detailu kontejneru vybraneho serveru
export const getContainerDetail = (token, env, id) => {
    return {
        type: c.GET_CONTAINER_DETAIL,
        payload: {
            request: {
                url: `/api/${env}/containers/${id}/json`,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        },
    };
}

// akce pro ziskani detailu kontejneru vybraneho serveru
export const startContainer = (token, env, id) => {
    return {
        type: c.START_CONTAINER,
        payload: {
            request: {
                method: 'post',
                url: `/api/${env}/containers/${id}/start`,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        },
    };
}

// akce pro ziskani detailu kontejneru vybraneho serveru
export const stopContainer = (token, env, id) => {
    return {
        type: c.STOP_CONTAINER,
        payload: {
            request: {
                method: 'post',
                url: `/api/${env}/containers/${id}/stop`,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        },
    };
}

// akce pro ziskani detailu kontejneru vybraneho serveru
export const removeContainer = (token, env, id) => {
    return {
        type: c.REMOVE_CONTAINER,
        payload: {
            request: {
                method: 'delete',
                url: `/api/${env}/containers/${id}?force=true`,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        },
    };
}

// reducer - zpracovani akci a jejich vysledku
const reducer = (state = initialState, action) => {
    switch (action.type) {
    case c.GET_CONTAINERS:
        return {
            ...state,
            loading: true,
        };
    case c.GET_CONTAINERS_SUCCESS:
        return {
            ...state,
            loading: false,
            containers: action.payload.data.response.data,
        };
    case c.GET_CONTAINERS_FAIL:
        return {
            ...state,
            loading: false,
            error: 'Error while fetching containers',
        };
    case c.GET_CONTAINER_DETAIL:
        return {
            ...state,
            loading: true,
        };
    case c.GET_CONTAINER_DETAIL_SUCCESS:
        return {
            ...state,
            loading: false,
            containerDetail: storeContainerInfo(action.payload.data.response.data),
        };
    case c.GET_CONTAINER_DETAIL_FAIL:
        return {
            ...state,
            loading: false,
            error: 'Error while fetching container detail',
        };
    case c.START_CONTAINER:
        return {
            ...state,
        };
    case c.START_CONTAINER_SUCCESS:
        return {
            ...state,
        };
    case c.START_CONTAINER_FAIL:
        return {
            ...state,
        };
    case c.STOP_CONTAINER:
        return {
            ...state,
        };
    case c.STOP_CONTAINER_SUCCESS:
        return {
            ...state,
        };
    case c.STOP_CONTAINER_FAIL:
        return {
            ...state,
        };
    case c.REMOVE_CONTAINER:
        return {
            ...state,
        };
    case c.REMOVE_CONTAINER_SUCCESS:
        return {
            ...state,
        };
    case c.REMOVE_CONTAINER_FAIL:
        return {
            ...state,
        };
    default:
        return state;
    }
};

export default reducer;
