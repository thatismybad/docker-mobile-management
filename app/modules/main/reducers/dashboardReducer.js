/**
 * Trida obsluhujici redux cast pro prehled main modulu - styl podle Ducks vzoru
 */
import * as c from '../constants';
import { storeServerInfo } from '../utils/serverInfoHandler';

// pocatecni stav
const initialState = {
    loading: false,
    loadingServerInfo: false,
    envLoaded: false,
    envInfo: [],
    envDetail: {},
    serverInfo: {
        containers: {
            total: 0,
            running: 0,
            stopped: 0,
        },
        images: 0,
    },
};

// akce pro ziskani informaci o konkretnim prostredi
export const getEnvInfo = (token, env) => {
    console.log(token, env)
    return {
        env,
        type: c.GET_ENV_INFO,
        payload: {
            request: {
                url: `/env/info/${env}/`,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        },
    };
};

// ziskani informaci o vsech prostredich
export const getAllEnvInfo = (token) => {
    return {
        type: c.GET_ALL_ENV_INFO,
        token,
        payload: {
            request: {
                url: '/env/all',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        },
    };
};

// ziskani informaci o konkretnim serveru
export const getServerInfo = (token, env) => {
    return {
        type: c.GET_SERVER_INFO,
        payload: {
            request: {
                url: `/api/${env}/info`,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        },
    };
};

// akce pro nastaveni informaci o serveru do stavu
export const setServerInfo = (data) => {
    return {
        type: c.SET_SERVER_INFO,
        data,
    };
};

// akce pro vymazani informaci o serveru ze stavu
export const setEmptyServerInfo = () => {
    return {
        type: c.SET_EMPTY_SERVER_INFO,
    };
};

export const turnOffEnvLoaded = () => {
    return {
        type: c.TURN_OFF_ENV_LOADED,
    };
};

// reducer - zpracovani akci a jejich vysledku
const reducer = (state = initialState, action) => {
    switch (action.type) {
    case c.GET_ENV_INFO:
        return {
            ...state,
            loading: true,
        };
    case c.GET_ENV_INFO_SUCCESS:
        return {
            ...state,
            loading: false,
            envDetail: action.payload.data.endpoint,
        };
    case c.GET_ENV_INFO_FAIL:
        return {
            ...state,
            loading: false,
            error: 'Error while fetching server info',
        };
    case c.GET_ALL_ENV_INFO:
        return {
            ...state,
            loading: true,
        };
    case c.GET_ALL_ENV_INFO_SUCCESS:
        return {
            ...state,
            loading: false,
            envLoaded: true,
            envInfo: action.payload.data.endpoints,
        };
    case c.GET_ALL_ENV_INFO_FAIL:
        return {
            ...state,
            loading: false,
            error: 'Error while fetching server info',
        };
    case c.GET_SERVER_INFO:
        return {
            ...state,
            loadingServerInfo: true,
        };
    case c.GET_SERVER_INFO_SUCCESS:
        return {
            ...state,
            loadingServerInfo: false,
            serverInfo: storeServerInfo(action.payload.data.response.data),
        };
    case c.GET_SERVER_INFO_FAIL:
        return {
            ...state,
            loadingServerInfo: false,
            error: 'Error while fetching server info',
        };
    case c.SET_SERVER_INFO:
        return {
            ...state,
            serverInfo: action.data,
        };
    case c.SET_ENV_LOADED_TO_FALSE:
        return {
            ...state,
            envLoaded: false,
        };
    case c.SET_EMPTY_SERVER_INFO:
        return {
            ...state,
            serverInfo: {
                containers: {
                    total: 0,
                    running: 0,
                    stopped: 0,
                },
                images: 0,
            },
        };
    default:
        return state;
    }
};

export default reducer;
