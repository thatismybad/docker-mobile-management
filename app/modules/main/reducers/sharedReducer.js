/**
 * Trida obsluhujici redux pro sdilene casti main modulu - styl podle Ducks vzoru
 */
import * as gC from '../../../utils/constants';
import * as c from '../constants';

const initialState = {
    selectedServer: gC.SERVER_DEFAULT,
    requestToken: '',
};

// vyber serveru pro server picker
export const setSelectedServer = (server) => {
    return {
        type: c.SET_SELECTED_SERVER,
        server,
    };
};

// ulozeni req. tokenu do storu
export const setRequestToken = (token) => {
    return {
        type: c.SET_REQUEST_TOKEN,
        token,
    }
}

// reducer - zpracovani akci a jejich vysledku
const reducer = (state = initialState, action) => {
    switch (action.type) {
    case c.SET_SELECTED_SERVER:
        return {
            ...state,
            selectedServer: action.server,
        };
    case c.SET_REQUEST_TOKEN:
        return {
            ...state,
            requestToken: action.token,
        }
    default:
        return state;
    }
};

export default reducer;
