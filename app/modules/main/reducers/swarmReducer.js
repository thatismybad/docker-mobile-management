/**
 * Trida obsluhujici redux cast pro swarm main modulu - styl podle Ducks vzoru
 */
import * as c from '../constants';
import storeNodesInfo from '../utils/nodesHandler';

// pocatecni stav
const initialState = {
    nodes: [],
    code: 0,
    error: '',
};

// akce pro ziskani informaci o nodech ve swarmu
export const getNodesInfo = (token, env) => {
    console.log(token, env)
    return {
        env,
        type: c.GET_NODES_INFO,
        payload: {
            request: {
                url: `/api/${env}/nodes`,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        },
    };
};

// reducer - zpracovani akci a jejich vysledku
const reducer = (state = initialState, action) => {
    switch (action.type) {
    case c.GET_NODES_INFO:
        return {
            ...state,
            error: '',
        };
    case c.GET_NODES_INFO_SUCCESS:
        return {
            ...state,
            error: '',
            nodes: storeNodesInfo(action.payload.data.response.data),
        };
    case c.GET_NODES_INFO_FAIL:
        return {
            ...state,
            code: action.error.response.data.response.code,
            error: action.error.response.data.response.message,
        };
    default:
        return state;
    }
};

export default reducer;
