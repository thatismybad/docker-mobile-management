/**
 * Trida obsluhujici redux cast pro obrazy main modulu - styl podle Ducks vzoru
 */
import * as c from '../constants';
import storeImageInfo from '../utils/imageHandler';

// pocatecni stav
const initialState = {
    loading: false,
    images: [],
    imageDetail: {},
    searchResults: [],
    message: '',
};

// akce pro ziskani seznamu obrazu vybraneho serveru
export const getImages = (token, env) => {
    return {
        type: c.GET_IMAGES,
        payload: {
            request: {
                url: `/api/${env}/images/json?all=1`,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        },
    };
};

// akce pro ziskani detailu obrazu vybraneho serveru
export const getImageDetail = (token, env, id) => {
    return {
        type: c.GET_IMAGE_DETAIL,
        payload: {
            request: {
                url: `/api/${env}/images/${id}/json`,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        },
    };
}

// akce pro smazani obrazu vybraneho serveru
export const removeImage = (token, env, id) => {
    return {
        type: c.REMOVE_IMAGE,
        payload: {
            request: {
                method: 'delete',
                url: `/api/${env}/images/${id}?force=true`,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        },
    };
}

// akce pro vyhledani obrazu 
export const searchForImage = (token, env, term) => {
    return {
        type: c.SEARCH_IMAGE,
        payload: {
            request: {
                url: `/api/${env}/images/search?term=${term}&limit=10`,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        },
    };
};

// akce pro stazeni obrazu 
export const pullImage = (token, env, image) => {
    return {
        type: c.PULL_IMAGE,
        payload: {
            request: {
                method: 'post',
                url: `/api/${env}/images/create?fromImage=${image}:latest`,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        },
    };
};

// reducer - zpracovani akci a jejich vysledku
const reducer = (state = initialState, action) => {
    switch (action.type) {
    case c.GET_IMAGES:
        return {
            ...state,
            loading: true,
        };
    case c.GET_IMAGES_SUCCESS:
        return {
            ...state,
            loading: false,
            images: action.payload.data.response.data,
        };
    case c.GET_IMAGES_FAIL:
        return {
            ...state,
            loading: false,
            error: 'Error while fetching images',
        };
    case c.GET_IMAGE_DETAIL:
        return {
            ...state,
            loading: true,
        };
    case c.GET_IMAGE_DETAIL_SUCCESS:
        return {
            ...state,
            loading: false,
            imageDetail: storeImageInfo(action.payload.data.response.data),
        };
    case c.GET_IMAGE_DETAIL_FAIL:
        return {
            ...state,
            loading: false,
            error: 'Error while fetching image detail',
        };
    case c.REMOVE_IMAGE:
        return {
            ...state,
            loading: true,
        };
    case c.REMOVE_IMAGE_SUCCESS:
        return {
            ...state,
            loading: false,
        };
    case c.REMOVE_IMAGE_FAIL:
        return {
            ...state,
            loading: false,
            error: 'Error while deleting image',
        };
    case c.SEARCH_IMAGE:
        return {
            ...state,
            loading: true,
        };
    case c.SEARCH_IMAGE_SUCCESS:
        return {
            ...state,
            loading: false,
            searchResults: action.payload.data.response.data
        };
    case c.SEARCH_IMAGE_FAIL:
        return {
            ...state,
            loading: false,
            error: 'Error while searching',
        };
    case c.PULL_IMAGE:
        return {
            ...state,
            loading: true,
        };
    case c.PULL_IMAGE_SUCCESS:
        return {
            ...state,
            loading: false,
            message: action.payload
        };
    case c.PULL_IMAGE_FAIL:
        return {
            ...state,
            loading: false,
            error: 'Error while pulling',
        };
    default:
        return state;
    }
};

export default reducer;
