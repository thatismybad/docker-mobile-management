import containerReducer from './reducers/containerReducer';
import dashboardReducer from './reducers/dashboardReducer';
import imageReducer from './reducers/imageReducer';
import swarmReducer from './reducers/swarmReducer';
import sharedReducer from './reducers/sharedReducer';

export {
    containerReducer,
    dashboardReducer,
    imageReducer,
    swarmReducer,
    sharedReducer,
};
