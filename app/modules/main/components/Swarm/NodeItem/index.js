import React, { Component } from 'react';
import {
    View, Text,
} from 'react-native';
import styles from './styles';

// komponenta pro prehled nodu obrazovky swarm
export default class NodeItem extends Component {
    render() {
        const {
            hostname, platform, resources, engineVersion,
            availability, role, address
        } = this.props.node;

        return (
            <View style={styles.container}>
                <Text style={styles.title}>{hostname}</Text>
                <Text>Role: {role}</Text>
                <Text>Dostupnost: { availability === 'active' ? 'dostupný' : 'nedostupný'}</Text>
                <Text>Adresa: {address}</Text>
                <Text>Verze: {engineVersion}</Text>
                <Text>OS/arch.: {platform.os} - {platform.arch}</Text>
                <View style={styles.resources}>
                    <Text>CPU: {resources.cpu}</Text>
                    <Text>Paměť: {resources.memory}</Text>
                </View>
            </View>
        );
    }
}
