import { StyleSheet } from 'react-native';
import * as c from '../../../../../utils/colors';

const styles = StyleSheet.create({
    container: {
        flexWrap: 'wrap',
        margin: 10,
        paddingLeft: 20,
        paddingBottom: 10,
        borderColor: c.colorWhite,
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: c.colorWhite,
        shadowColor: c.colorBlack,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    title: {
        color: c.darkerGrey60,
        fontSize: 18,
        margin: 10,
    },
    resources: {
        flexDirection: 'column',
    }
});

export default styles;
