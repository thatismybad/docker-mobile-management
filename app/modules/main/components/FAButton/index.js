import React from 'react';
import { View } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';

import { PLUS } from '../../../../utils/icons';
import styles from './styles';
import * as c from '../../../../utils/colors';

const FAButton = () => {
    return (
        <View style={styles.button}>
            <Feather
                name={PLUS}
                size={25}
                color={c.colorWhite} />
        </View>
    )
}

export default FAButton;
