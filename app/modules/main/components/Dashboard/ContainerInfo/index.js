import React from 'react';
import { View, Text } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import * as c from '../../../../../utils/colors';
import * as i from '../../../../../utils/icons';
import styles from './styles';

const ContainerInfo = (props) => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Kontejnery</Text>
            <View style={styles.infoPart}>
                <View style={styles.icon}>
                    <Feather
                        name={i.CONTAINERS}
                        color={c.colorWhite}
                        size={30}
                    />
                </View>
                <View style={styles.text}>
                    <Text style={styles.running}>Spuštěné: {props.data.running}</Text>
                    <Text style={styles.stopped}>Zastavené: {props.data.stopped}</Text>
                </View>
            </View>
        </View>
    );
};

export default ContainerInfo;
