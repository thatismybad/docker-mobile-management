import React, { Component } from 'react';
import {
    View, Text, TouchableOpacity, ActivityIndicator,
} from 'react-native';
import { connect } from 'react-redux';

import styles from './styles';
import gStyles from '../../../../../styles/theme';
import ServerPicker from '../../ServerPicker';
import ContainerInfo from '../ContainerInfo';
import ImageInfo from '../ImageInfo';

class ServerInfo extends Component {
    renderItem = ({ item }) => (
        <TouchableOpacity
            style={styles.item}
            onPress={() => console.log(item.name)}
        >
            <View style={styles.itemContent}>
                <Text style={styles.itemText}>{item.name}</Text>
            </View>
        </TouchableOpacity>
    );

    renderSeparator = () => <View style={styles.divider} />

    render() {
        const { loadingServerInfo, serverInfo } = this.props.dashboard;
        return (
            <View style={styles.container}>
                <Text style={styles.title}>Komponenty</Text>
                <ServerPicker screen="dash"/>
                { loadingServerInfo
                    ?
                    <ActivityIndicator style={styles.components} />
                    :
                    <View style={styles.components}>
                        <ContainerInfo data={serverInfo.containers} />
                        <ImageInfo data={serverInfo.images} />
                    </View>
                }
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        dashboard: state.dashboardReducer,
        shared: state.sharedReducer,
    };
};

export default connect(mapStateToProps, null)(ServerInfo);
