import { StyleSheet } from 'react-native';
import * as c from '../../../../../utils/colors';

const styles = StyleSheet.create({
    container: {
        flexWrap: 'wrap',
        margin: 10,
        borderColor: c.colorWhite,
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: c.colorWhite,
        shadowColor: c.colorBlack,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    title: {
        color: c.darkerGrey60,
        fontSize: 18,
        margin: 10,
    },
    list: {
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
    },
    item: {
        height: 40,
        borderColor: c.lightGrey120,
        paddingLeft: 10,
        paddingRight: 10,
        justifyContent: 'center',
    },
    itemContent: {
        paddingTop: 5,
        paddingBottom: 5,
        flexDirection: 'row',
        alignContent: 'center',
    },
    itemText: {
        paddingLeft: 15,
        fontSize: 15,
    },
    divider: {
        height: 1,
        width: '100%',
        backgroundColor: c.lightGrey130,
    },
    components: {
        flexDirection: 'row',
    },
});

export default styles;
