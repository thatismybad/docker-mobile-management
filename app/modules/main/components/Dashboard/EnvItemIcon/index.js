import React from 'react';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import * as c from '../../../../../utils/colors';

const EnvItemIcon = (props) => {
    const os = props.os === 'Linux' ? 'linux' : 'windows';
    return (
        <FontAwesome5
            name={os}
            color={c.darkerGrey60}
            size={18}
        />
    );
};

export default EnvItemIcon;
