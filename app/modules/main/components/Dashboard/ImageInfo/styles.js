import { StyleSheet } from 'react-native';
import * as c from '../../../../../utils/colors';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        margin: 10,
        paddingBottom: 5,
    },
    infoPart: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 10,
    },
    text: {
        flex: 1,
    },
    icon: {
        backgroundColor: c.colorPurple,
        borderRadius: 50,
        padding: 10,
        marginRight: 10,
    },
    title: {
        color: c.darkerGrey60,
        fontSize: 18,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 5,
    },
});

export default styles;
