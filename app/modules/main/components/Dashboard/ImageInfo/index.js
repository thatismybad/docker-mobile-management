import React from 'react';
import { View, Text } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import * as c from '../../../../../utils/colors';
import * as i from '../../../../../utils/icons';
import styles from './styles';

const ImageInfo = (props) => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Obrazy</Text>
            <View style={styles.infoPart}>
                <View style={styles.icon}>
                    <Feather
                        name={i.IMAGES}
                        color={c.colorWhite}
                        size={30}
                    />
                </View>
                <View style={styles.text}>
                    <Text>Celkem: {props.data}</Text>
                </View>
            </View>
        </View>
    );
};

export default ImageInfo;
