import React from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
import ComponentIcon from '../ComponentIcon';
import * as c from '../../../../../utils/colors';
import styles from './styles';

const ImagesItem = (props) => {
    return (
        <TouchableOpacity
            style={styles.item}
            onPress={() => console.log(item.name)}
        >
            <View style={styles.itemContent}>
                <ComponentIcon icon="images" />
                <Text style={styles.itemText}>{item.name}</Text>
            </View>
        </TouchableOpacity>
    );
};

export default ImagesItem;
