import React, { Component } from 'react';
import {
    View, Text, TouchableOpacity, FlatList,
} from 'react-native';
import EnvItemIcon from '../EnvItemIcon';
import styles from './styles';
import { ENV_DETAIL_ROUTE } from '../../../../../utils/constants';

export default class EnvInfo extends Component {
    renderItem = ({ item }) => (
        <TouchableOpacity
            style={styles.item}
            onPress={() => {
                this.props.nav.navigate(ENV_DETAIL_ROUTE, { env: item.env })
            }}
        >
            <View style={styles.itemContent}>
                <EnvItemIcon style={styles.icon} os={item.os} />
                <Text style={styles.itemText}>{item.name}</Text>
            </View>
        </TouchableOpacity>
    );

    renderSeparator = () => <View style={styles.divider} />

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title}>Prostředí</Text>
                <FlatList
                    style={styles.list}
                    data={this.props.envInfo}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => `${index}`}
                    ItemSeparatorComponent={this.renderSeparator}
                />
            </View>
        );
    }
}
