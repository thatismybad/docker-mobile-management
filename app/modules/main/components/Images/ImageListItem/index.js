import React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';

const ImageListItem = (data) => {
    const {
        Id, RepoTags,
    } = data.image;
    const id = Id.substring(7, 17);
    const name = RepoTags[0];
    return (
        <View style={styles.container}>
            <View style={styles.text}>
                <Text style={styles.name}>{name}</Text>
                <View style={styles.info}>
                    <Text style={styles.id}>{id}</Text>
                </View>
            </View>
        </View>
    );
};

export default ImageListItem;
