import React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';
import Feather from 'react-native-vector-icons/Feather';

const ImageSearchItem = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.text}>
                <Text style={styles.name}>{props.image.name}</Text>
                <View style={styles.info}>
                    <Feather name="star" size={25} color="orange" />
                    <Text style={styles.stars}>{props.image.star_count}</Text>
                </View>
            </View>
        </View>
    );
};

export default ImageSearchItem;
