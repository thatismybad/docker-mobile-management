import { StyleSheet } from 'react-native';
import * as c from '../../../../../utils/colors';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: c.colorWhite,
        alignItems: 'center',
    },
    text: {
        flex: 1,
        flexDirection: 'column',
        padding: 10,
        marginRight: 10,
    },
    name: {
        flex: 1,
        marginBottom: 5,
        fontSize: 15,
        fontWeight: 'bold',
    },
    stars: {
        marginLeft: 5,
    },
    info: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
});
export default styles;
