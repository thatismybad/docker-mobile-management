import { StyleSheet } from 'react-native';
import * as c from '../../../../../utils/colors';

const styles = (running = true) => {
    return StyleSheet.create({
        container: {
            flex: 1,
            flexDirection: 'row',
            backgroundColor: running ? c.colorWhite : c.lightGrey140,
            alignItems: 'center',
        },
        text: {
            flex: 1,
            flexDirection: 'column',
            padding: 10,
            marginRight: 10,
        },
        name: {
            flex: 1,
            marginBottom: 5,
        },
        info: {
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
        },
        id: {
            color: c.lightGrey120,
        },
        image: {
            color: c.lightGrey120,
        },
        icon: {
            marginRight: 15,
        },
    });
};

export default styles;
