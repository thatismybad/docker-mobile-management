import React from 'react';
import { View, Text } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import * as c from '../../../../../utils/colors';
import styles from './styles';

const ContainerListItem = (data) => {
    const {
        Id, Names,
        Image, State,
    } = data.container;
    const id = Id.substring(0, 10);
    const name = Names[0].substring(1);
    const running = State === 'running';
    const icon = running ? 'play' : 'stop';
    const color = running ? c.colorGreen : c.colorRed;
    const style = styles(running);
    return (
        <View style={style.container}>
            <View style={style.text}>
                <Text style={style.name}>{name}</Text>
                <View style={style.info}>
                    <Text style={style.id}>{id}</Text>
                    <Text style={style.image}>{Image}</Text>
                </View>
            </View>
            <FontAwesome5 name={icon} style={style.icon} size={20} color={color} />
        </View>
    );
};

export default ContainerListItem;
