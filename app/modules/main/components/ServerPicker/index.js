import React, { PureComponent } from 'react';
import { View, Text, Picker } from 'react-native';
import { connect } from 'react-redux';

import * as s from '../../../../utils/strings';
import * as c from '../../../../utils/constants';
import styles from './styles';

import { setSelectedServer } from '../../reducers/sharedReducer';
import { getServerInfo, setEmptyServerInfo } from '../../reducers/dashboardReducer';
import { getContainers } from '../../reducers/containerReducer';
import { getImages } from '../../reducers/imageReducer';
import { getNodesInfo } from '../../reducers/swarmReducer';

class ServerPicker extends PureComponent {

    componentDidMount() {
        const { selectedServer } = this.props.shared;
        this.handleValue(selectedServer);
    }

    handleValue(value) {
        const { requestToken } = this.props.shared;
        const { screen } = this.props;
        if (value !== c.SERVER_DEFAULT) {
            switch (screen) {
            case 'dash':
                this.props.getServerInfo(requestToken, value);
                break;
            case 'cont':
                this.props.getContainers(requestToken, value);
                break;
            case 'imag':
                this.props.getImages(requestToken, value);
                break;
            case 'swrm':
                this.props.getNodesInfo(requestToken, value);
                break;
            default: console.log('do nothing');
                break;
            }
        } else {
            this.props.setEmptyServerInfo();
        }
        this.props.setSelectedServer(value);
    }

    render() {
        const { selectedServer } = this.props.shared;        
        const { envInfo } = this.props.dashboard;        

        return (
            <View style={styles.container}>
                <Text style={styles.title}>Server:</Text>
                <Picker
                    style={styles.picker}
                    itemStyle={styles.itemStyle}
                    selectedValue={selectedServer}
                    onValueChange={(itemValue) => {
                        this.handleValue(itemValue);
                    }}
                >
                    <Picker.Item label={s.DEFAULT_SELECTION_ALL} value={c.SERVER_DEFAULT} key={c.SERVER_DEFAULT} />
                    {
                        envInfo.map((item) => {
                            return <Picker.Item label={item.name} value={item.env} key={item.env} />;
                        })
                    }
                </Picker>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        container: state.containerReducer,
        dashboard: state.dashboardReducer,
        shared: state.sharedReducer,
    };
};

export default connect(mapStateToProps,
    {
        getServerInfo,
        setSelectedServer,
        setEmptyServerInfo,
        getContainers,
        getImages,
        getNodesInfo,
    })(ServerPicker);
