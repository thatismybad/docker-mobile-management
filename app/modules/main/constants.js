// trida s konstantami pro reducer main modulu
export const GET_ENV_INFO = 'GET_ENV_INFO';
export const GET_ENV_INFO_SUCCESS = 'GET_ENV_INFO_SUCCESS';
export const GET_ENV_INFO_FAIL = 'GET_ENV_INFO_FAIL';

export const GET_ALL_ENV_INFO = 'GET_ALL_ENV_INFO';
export const GET_ALL_ENV_INFO_SUCCESS = 'GET_ALL_ENV_INFO_SUCCESS';
export const GET_ALL_ENV_INFO_FAIL = 'GET_ALL_ENV_INFO_FAIL';

export const GET_SERVER_INFO = 'GET_SERVER_INFO';
export const GET_SERVER_INFO_SUCCESS = 'GET_SERVER_INFO_SUCCESS';
export const GET_SERVER_INFO_FAIL = 'GET_SERVER_INFO_FAIL';

export const SET_SERVER_INFO = 'SET_SERVER_INFO';
export const SET_SELECTED_SERVER = 'SET_SELECTED_SERVER';
export const SET_REQUEST_TOKEN = 'SET_REQUEST_TOKEN';
export const SET_EMPTY_SERVER_INFO = 'SET_EMPTY_SERVER_INFO';
export const TURN_OFF_ENV_LOADED = 'TURN_OFF_ENV_LOADED';

export const GET_CONTAINERS = 'GET_CONTAINERS';
export const GET_CONTAINERS_SUCCESS = 'GET_CONTAINERS_SUCCESS';
export const GET_CONTAINERS_FAIL = 'GET_CONTAINERS_FAIL';

export const GET_CONTAINER_DETAIL = 'GET_CONTAINER_DETAIL';
export const GET_CONTAINER_DETAIL_SUCCESS = 'GET_CONTAINER_DETAIL_SUCCESS';
export const GET_CONTAINER_DETAIL_FAIL = 'GET_CONTAINER_DETAIL_FAIL';

export const GET_IMAGES = 'GET_IMAGES';
export const GET_IMAGES_SUCCESS = 'GET_IMAGES_SUCCESS';
export const GET_IMAGES_FAIL = 'GET_IMAGES_FAIL';

export const GET_IMAGE_DETAIL = 'GET_IMAGE_DETAIL';
export const GET_IMAGE_DETAIL_SUCCESS = 'GET_IMAGE_DETAIL_SUCCESS';
export const GET_IMAGE_DETAIL_FAIL = 'GET_IMAGE_DETAIL_FAIL';

export const REMOVE_IMAGE = 'REMOVE_IMAGE';
export const REMOVE_IMAGE_SUCCESS = 'REMOVE_IMAGE_SUCCESS';
export const REMOVE_IMAGE_FAIL = 'REMOVE_IMAGE_FAIL';

export const SEARCH_IMAGE = 'SEARCH_IMAGE';
export const SEARCH_IMAGE_SUCCESS = 'SEARCH_IMAGE_SUCCESS';
export const SEARCH_IMAGE_FAIL = 'SEARCH_IMAGE_FAIL';

export const PULL_IMAGE = 'PULL_IMAGE';
export const PULL_IMAGE_SUCCESS = 'PULL_IMAGE_SUCCESS';
export const PULL_IMAGE_FAIL = 'PULL_IMAGE_FAIL';

export const GET_NODES_INFO = 'GET_NODES_INFO';
export const GET_NODES_INFO_SUCCESS = 'GET_NODES_INFO_SUCCESS';
export const GET_NODES_INFO_FAIL = 'GET_NODES_INFO_FAIL';

export const START_CONTAINER = 'START_CONTAINER';
export const START_CONTAINER_SUCCESS = 'START_CONTAINER_SUCCESS';
export const START_CONTAINER_FAIL = 'START_CONTAINER_FAIL';

export const STOP_CONTAINER = 'STOP_CONTAINER';
export const STOP_CONTAINER_SUCCESS = 'STOP_CONTAINER_SUCCESS';
export const STOP_CONTAINER_FAIL = 'STOP_CONTAINER_FAIL';

export const REMOVE_CONTAINER = 'REMOVE_CONTAINER';
export const REMOVE_CONTAINER_SUCCESS = 'REMOVE_CONTAINER_SUCCESS';
export const REMOVE_CONTAINER_FAIL = 'REMOVE_CONTAINER_FAIL';

