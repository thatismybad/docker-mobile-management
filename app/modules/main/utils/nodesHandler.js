// metoda ktera parsuje odpoved ze serveru na data o nodech, ktera jsou pro nas dulezita
const storeNodesInfo = (data) => {
    console.log(data)
    const nodes = [];
    data.map((node) => {
        let memory;
        if (node.Description.Resources.MemoryBytes / 1000000 > 1000) {
            memory = `${Number((node.Description.Resources.MemoryBytes) / 1000000000).toFixed(2)} GB`
        } else {
            memory = `${Number((node.Description.Resources.MemoryBytes) / 1000000).toFixed(2)} MB`
        }
        nodes.push({
            hostname: node.Description.Hostname,
            platform: {
                arch: node.Description.Platform.Architecture,
                os: node.Description.Platform.OS,
            },
            resources: {
                cpu: node.Description.Resources.NanoCPUs / 1000000000,
                memory,
            },
            engineVersion: node.Description.Engine.EngineVersion,
            availability: node.Spec.Availability,
            role: node.Spec.Role,
            address: node.Status.Addr,
        })
    });
    return nodes;
};

export default storeNodesInfo;
