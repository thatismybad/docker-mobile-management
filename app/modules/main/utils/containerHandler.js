// metoda ktera parsuje odpoved ze serveru na data o kontejneru, ktera jsou pro nas dulezita
const storeContainerInfo = (data) => {
    console.log(data)
    return {
        name: data.Name.substring(1, data.Name.length),
        id: data.Id,
        status: data.State.Status,
        parentImage: data.Config.Image,
        labels: data.Config.Labels,
        volumes: data.Config.Volumes,
        hostname: data.Config.Hostname,
        ports: [],
    };
};

export default storeContainerInfo;
