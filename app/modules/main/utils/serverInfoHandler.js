// metoda ktera parsuje odpoved ze serveru na data o Docker objektech na serveru, ktera jsou pro nas dulezita
export const storeServerInfo = (data) => {
    return {
        containers: {
            total: data.Containers,
            running: data.ContainersRunning,
            stopped: data.ContainersStopped,
        },
        images: data.Images,
    };
};

export const getAllServerInfo = (data) => {
    let numCont = 0;
    let numRunCont = 0;
    let numStopCont = 0;
    let numImages = 0;
    data.forEach((server) => {
        numCont += server.info.containers.total;
        numRunCont += server.info.containers.running;
        numStopCont += server.info.containers.stopped;
        numImages += server.info.images;
    });
    return {
        containers: {
            total: numCont,
            running: numRunCont,
            stopped: numStopCont,
        },
        images: numImages,
    };
};
