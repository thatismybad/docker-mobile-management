// metoda ktera parsuje odpoved ze serveru na data o obrazu, ktera jsou pro nas dulezita
const storeImageInfo = (data) => {
    console.log(data)
    let size;
    if (data.Size / 1000000 > 1000) {
        size = `${Number((data.Size) / 1000000000).toFixed(2)} GB`
    } else {
        size = `${Number((data.Size) / 1000000).toFixed(2)} MB`
    }
    return {
        name: data.RepoTags[0],
        id: data.Id.substring(7),
        arch: data.Architecture,
        os: data.Os,
        size,
    };
};

export default storeImageInfo;
