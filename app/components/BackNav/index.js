import React from 'react';
import { View, Text } from 'react-native';
import { NavigationActions } from 'react-navigation';
import styles from './styles';
import * as c from '../../utils/colors';
import Feather from 'react-native-vector-icons/Feather';

import { CH_LEFT } from '../../utils/icons';

// status bar komponenta
const BackNav = ({ nav, title }) => (
    <View style={styles.container}>
    <Feather
                style={styles.leftAction}
                name={CH_LEFT}
                size={30}
                color={c.iosBlue}
                onPress={() => {
                    nav.dispatch(NavigationActions.back());
                }
                }
            />
    <Text style={styles.title}>{title}</Text>
    </View>
);

export default BackNav;
