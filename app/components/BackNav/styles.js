import { StyleSheet } from 'react-native';
import * as c from '../../utils/colors';

const styles = StyleSheet.create({
    container: {
        height: 44,
        backgroundColor: c.colorWhite,
        marginBottom: 5,
        borderBottomWidth: 1,
        borderColor: c.lightGrey130,
        flexDirection: 'row',
        alignItems: 'center',
    },
    leftAction: {
        marginLeft: 10,
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold',
        alignItems: 'center',
    }
});

export default styles;
