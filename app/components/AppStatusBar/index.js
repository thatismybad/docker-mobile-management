import React from 'react';
import { View, StatusBar } from 'react-native';
import styles from './styles';

// status bar komponenta
const AppStatusBar = () => (
    <View style={styles.container}>
        <StatusBar />
    </View>
);

export default AppStatusBar;