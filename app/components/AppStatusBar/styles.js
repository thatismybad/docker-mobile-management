import { StyleSheet, Platform } from 'react-native';

const styles = StyleSheet.create({
    container: {
        height: (Platform.OS === 'ios') ? 20 : 0,
    },
});

export default styles;
