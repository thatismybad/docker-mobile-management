import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    loading: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
    },
    leftAction: {
        marginLeft: 10,
    },
    rightAction: {
        marginRight: 10,
    },
});

export default styles;
