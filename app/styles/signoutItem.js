import { StyleSheet } from 'react-native';
import * as c from '../utils/colors';

const styles = StyleSheet.create({
    item: {
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 5,
    },
    label: {
        margin: 14,
        marginLeft: 32,
        fontWeight: 'bold',
        fontSize: 14,
        color: c.colorBlackTransparent222,
    },
});

export default styles;
