// utilizacni trida pro praci s tokenem
import { JWT_TOKEN } from './constants';

// funkce pro ziskani tokenu z odpovedi
export const getToken = (token, type) => {
    const parts = token.split('.');
    return type === JWT_TOKEN ? `${parts[0]}.${parts[1]}.${parts[3]}` : parts[2];
};

// funkce pro sestaveni tokenu pro komunikaci
export const buildToken = (JWT, UQ) => {
    const JWTparts = JWT.split('.');
    return `${JWTparts[0]}.${UQ}.${JWTparts[1]}.${JWTparts[2]}`;
};
