// uzilizacni trida pro praci s asynchronim ulozistem - funkcionality predevsim pro overovani uzivatele
import { AsyncStorage } from 'react-native';

export const getStorageItem = async (item) => {
    const result = await AsyncStorage.getItem(item);
    return result;
};

export const getStorageItems = async (items) => {
    const results = [];
    results.push(await AsyncStorage.getItem(items[0]));
    results.push(await AsyncStorage.getItem(items[1]));
    return results;
};

export const setStorageItem = async (item, value) => {
    try {
        console.log('setStorageItem', item, value);
        await AsyncStorage.setItem(item, value);
    } catch (error) {
        console.log('AsyncStorage Error:', error.message);
    }
};

export const removeStorageItem = (item) => {
    try {
        AsyncStorage.removeItem(item);
    } catch (error) {
        console.log('AsyncStorage Error:', error.message);
    }
};

export const clearAsyncStorage = () => {
    AsyncStorage.clear();
};
