// globalni trida s konstantami projekti
export const ASYNC_ST_USER_KEY = 'st_user';
export const ASYNC_ST_TOKEN_KEY = 'st_token';
export const ASYNC_ST_UQ_TOKEN_KEY = 'st_uq_token';

export const SERVER_DEFAULT = 'none';

export const AUTH_ROUTE = 'Auth';
export const APP_ROUTE = 'App';
export const DASHBOARD_ROUTE = 'Dashboard';
export const CONTAINERS_ROUTE = 'Containers';
export const IMAGES_ROUTE = 'Images';
export const SWARM_ROUTE = 'Swarm';
export const SIGNOUT_ROUTE = 'SignOut';
export const ENV_DETAIL_ROUTE = 'EnvDetail';
export const IMAGE_DETAIL_ROUTE = 'ImageDetail';
export const CONTAINER_DETAIL_ROUTE = 'ContainerDetail';
export const IMAGE_ADD_ROUTE = 'ImageAdd';

export const JWT_TOKEN = 'JWT_TOKEN';
export const UQ_TOKEN = 'UQ_TOKEN';
