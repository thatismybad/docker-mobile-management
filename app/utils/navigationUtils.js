// utilizacni trida pro vytvoreni navigacnich prvku toolbaru jednotlivych platforem
import * as c from './colors';

export const createIOSNavOptions = (title, leftAction, rightAction) => ({
    headerTitle: title,
    headerLeft: leftAction,
    headerRight: rightAction,
});

export const createAndNavOptions = (title, leftAction) => ({
    headerTitle: title,
    headerLeft: leftAction,
    headerStyle: { backgroundColor: c.lightBlue },
    headerTintColor: c.colorWhite,
});
