// globalni trida s retezci v projektu
// menu labels
export const MENU_LABEL_DASHBOARD = 'Přehled';
export const MENU_LABEL_CONTAINERS = 'Kontejnery';
export const MENU_LABEL_IMAGES = 'Obrazy';
export const MENU_LABEL_SWARM = 'Swarm';
export const MENU_LABEL_SIGN_OUT = 'Odhlásit se';

// component labels
export const COMPONENT_CONTAINERS = 'Kontejnery';
export const COMPONENT_IMAGES = 'Obrazy';
export const COMPONENT_VOLUMES = 'Svazky';
export const COMPONENT_SERVICES = 'Služby';
export const COMPONENT_NETWORKS = 'Sítě';

// component messages
export const DEFAULT_SELECTION_ALL = '-';

// sign out messages
export const SIGN_OUT_TITLE = 'Opravdu se chcete odhlásit?';
export const SIGN_OUT_TEXT = '';

// dialog
export const DELETE_IMAGE_TITLE = 'Opravdu chcete obraz smazat?';
export const PULL_IMAGE_TITLE = 'Stažení obrazu...';
export const PULL_IMAGE_MSG = 'Opravdu chcete stáhnout obraz: ';
export const DELETE_CONTAINER_TITLE = 'Opravdu chcete kontejner smazat?';
export const EMPTY_MSG = '';

// login screen
export const LOGIN_USERNAME_HINT = 'Email / přihl. jméno';
export const LOGIN_PASSWORD_HINT = 'Heslo';
export const LOGIN_SIGNIN_BUTTON = 'Přihlásit';

// error messages
export const ERROR_NETWORK_CONNECTION_TITLE = 'Problém s připojením k síti';
export const ERROR_NETWORK_CONNECTION_TEXT = 'Zkontrolujte připojení k Internetu a zkuste to znovu';
export const ERROR_SIGN_IN_FAILED_TITLE = 'Přihlášení se nezdařilo';
export const ERROR_SIGN_IN_FAILED_TEXT = 'Zkontrolujte zadané jméno nebo heslo';
export const ERROR_UNKNOWN_ERROR_TITLE = 'Něco se asi posralo';
export const ERROR_UNKNOWN_ERROR_TEXT = 'A nebo to tak alespoň vypadá';

export const BTN_YES = 'Ano';
export const BTN_NO = 'Ne';
export const BTN_OK = 'OK';
export const BTN_CANCEL = 'Zrušit';
