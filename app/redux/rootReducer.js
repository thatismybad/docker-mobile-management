// trida pro nastaveni korenoveho reduceru
import { combineReducers } from 'redux';

import authReducer from '../modules/auth';
import {
    dashboardReducer,
    sharedReducer,
    containerReducer,
    imageReducer,
    swarmReducer,
} from '../modules/main';

const rootReducer = combineReducers({
    authReducer,
    dashboardReducer,
    sharedReducer,
    containerReducer,
    imageReducer,
    swarmReducer,
});

export default rootReducer;
