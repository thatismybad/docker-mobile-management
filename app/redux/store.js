// trida pro nastaveni redux storu a middleware klienta pro sitovou komunikaci
import { createStore, applyMiddleware } from 'redux';
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';

import reducers from './rootReducer';

const client = axios.create({
    baseURL: 'https://middlewhale.herokuapp.com',
    responseType: 'json',
});

export default createStore(reducers, applyMiddleware(axiosMiddleware(client)));
