import React from 'react';
import { createSwitchNavigator } from 'react-navigation';

import Auth from './config/routes/Auth';
import AppNav from './config/routes/AppNav';
import IndexScreen from './modules/auth/scenes/Index';

// korenova navigace aplikace 
export default createSwitchNavigator(
    {
        Index: IndexScreen,
        Auth,
        App: {
            screen: ({ navigation }) => <AppNav screenProps={navigation} />,
        },
    }, {
        initialRouteName: 'Index',
    },
);
