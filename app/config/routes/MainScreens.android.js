import React from 'react';
import { Alert, View, TouchableOpacity, Text } from 'react-native';
import { createDrawerNavigator, createStackNavigator, NavigationActions, DrawerItems } from 'react-navigation';
import Feather from 'react-native-vector-icons/Feather';

import { clearAsyncStorage } from '../../utils/authUtils';
import { createAndNavOptions } from '../../utils/navigationUtils';

import * as s from '../../utils/strings';
import * as i from '../../utils/icons';
import * as c from '../../utils/colors';
import * as gC from '../../utils/constants';
import signoutStyle from '../../styles/signoutItem';

import Dashboard from '../../modules/main/scenes/Dashboard';
import Containers from '../../modules/main/scenes/Containers';
import Images from '../../modules/main/scenes/Images';
import Swarm from '../../modules/main/scenes/Swarm';
import styles from '../../styles/theme';

// funkce pro vraceni akce pro toolbar
const setNavAction = (navigation, back = true) => {
    return back ? <BackNav nav={navigation} /> : <DrawerNav nav={navigation} />;
};

// navigacni komponenta pro otevreni navigation draweru
const DrawerNav = ({ nav }) => (
    <Feather
        style={styles.leftAction}
        name={i.MENU}
        size={30}
        color={c.colorWhite}
        onPress={() => nav.toggleDrawer()}
    />
);

// navigacni komponenta pro akci zpet
const BackNav = ({ nav }) => (
    <Feather
        style={styles.leftAction}
        name={i.BACK}
        size={30}
        color={c.colorWhite}
        onPress={() => {
            nav.dispatch(NavigationActions.back());
        }
        }
    />
);

// komponenta logout tlacitka s obsluhou odhlaseni uzivatele
const DrawerWithLogoutButton = props => (
    <View>
        <DrawerItems {...props} />
        <TouchableOpacity
            onPress={() => {
                return Alert.alert(
                    s.SIGN_OUT_TITLE,
                    s.SIGN_OUT_TEXT,
                    [
                        {
                            text: s.BTN_OK,
                            onPress: () => {
                                props.screenProps.screenProps.navigate(gC.AUTH_ROUTE);
                                clearAsyncStorage();
                            },
                        },
                        { text: s.BTN_CANCEL },
                    ],
                );
            }}
        >
            <View style={signoutStyle.item}>
                <View>
                    <Feather
                        style={styles.leftAction}
                        name={i.SIGNOUT}
                        size={25}
                        color={c.darkerGrey120}
                    />
                </View>
                <Text style={signoutStyle.label}>{s.MENU_LABEL_SIGN_OUT}</Text>
            </View>
        </TouchableOpacity>
    </View>
);

// navigace pro Android navigation drawer - kazdy prvek obaleny stack navigatorem pro dodatecne moznosti navigace
export default createDrawerNavigator({
    Dashboard: {
        screen: createStackNavigator({
            Dashboard: {
                screen: Dashboard,
                navigationOptions: ({ navigation }) => createAndNavOptions(
                    s.MENU_LABEL_DASHBOARD,
                    setNavAction(navigation, false),
                ),
            },
        }),
        navigationOptions: {
            drawerLabel: s.MENU_LABEL_DASHBOARD,
            drawerIcon: ({ tintColor }) => <Feather name={i.DASHBOARD} size={25} color={tintColor} />,
        },
    },
    Containers: {
        screen: createStackNavigator({
            Containers: {
                screen: Containers,
                navigationOptions: ({ navigation }) => createAndNavOptions(
                    s.MENU_LABEL_CONTAINERS,
                    setNavAction(navigation, false),
                ),
            },
        }),
        navigationOptions: {
            drawerLabel: s.MENU_LABEL_CONTAINERS,
            drawerIcon: ({ tintColor }) => <Feather name={i.CONTAINERS} size={25} color={tintColor} />,
        },
    },
    Images: {
        screen: createStackNavigator({
            Images: {
                screen: Images,
                navigationOptions: ({ navigation }) => createAndNavOptions(
                    s.MENU_LABEL_IMAGES,
                    setNavAction(navigation, false),
                ),
            },
        }),
        navigationOptions: {
            drawerLabel: s.MENU_LABEL_IMAGES,
            drawerIcon: ({ tintColor }) => <Feather name={i.IMAGES} size={25} color={tintColor} />,
        },
    },
    Swarm: {
        screen: createStackNavigator({
            Swarm: {
                screen: Swarm,
                navigationOptions: ({ navigation }) => createAndNavOptions(
                    s.MENU_LABEL_SWARM,
                    setNavAction(navigation, false),
                ),
            },
        }),
        navigationOptions: {
            drawerLabel: s.MENU_LABEL_SWARM,
            drawerIcon: ({ tintColor }) => <Feather name={i.SWARM} size={25} color={tintColor} />,
        },
    },
}, {
    contentComponent: DrawerWithLogoutButton,
    drawerOptions: {
        activeTintColor: c.iosBlue,
        inactiveTintColor: c.darkerGrey120,
    },
    initialRouteName: gC.DASHBOARD_ROUTE,
});
