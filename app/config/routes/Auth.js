import { createStackNavigator } from 'react-navigation';

import SignIn from '../../modules/auth/scenes/SignIn';

// navigace pro neautentifikovanou cast aplikace - prihlasovaci obrazovka
export default createStackNavigator({
    SignIn: {
        screen: SignIn,
    },
});
