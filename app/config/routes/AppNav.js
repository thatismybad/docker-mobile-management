import React from 'react';
import { createStackNavigator } from 'react-navigation';

import MainScreens from './MainScreens';

import ContainerDetail from '../../modules/main/scenes/ContainerDetail';
import EnvDetail from '../../modules/main/scenes/EnvDetail';
import ImageAdd from '../../modules/main/scenes/ImageAdd';
import ImageDetail from '../../modules/main/scenes/ImageDetail';

// navigace pro autentifikovanou cast aplikace
export default createStackNavigator({
    MainScreens: {
        screen: ({ screenProps, navigation }) => <MainScreens screenProps={{ screenProps, navigation }} />,
    },
    ContainerDetail: {
        screen: ContainerDetail,
    },
    EnvDetail: {
        screen: EnvDetail,
    },
    ImageAdd: {
        screen: ImageAdd,
    },
    ImageDetail: {
        screen: ImageDetail,
    },
}, {
    headerMode: 'none',
});