import React from 'react';
import { Alert } from 'react-native';
import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import Feather from 'react-native-vector-icons/Feather';

import * as c from '../../utils/colors';
import * as s from '../../utils/strings';
import * as i from '../../utils/icons';

import * as gC from '../../utils/constants';
import styles from '../../styles/theme';

import { clearAsyncStorage } from '../../utils/authUtils';
import { createIOSNavOptions } from '../../utils/navigationUtils';

import SignIn from '../../modules/auth/scenes/SignIn';

import Dashboard from '../../modules/main/scenes/Dashboard';
import Containers from '../../modules/main/scenes/Containers';
import Images from '../../modules/main/scenes/Images';
import Swarm from '../../modules/main/scenes/Swarm';

// navigace pro Android navigation drawer - kazdy prvek obaleny stack navigatorem pro dodatecne moznosti navigace
export default createBottomTabNavigator({
    Dashboard: {
        screen: createStackNavigator({
            Dashboard: {
                screen: Dashboard,
                navigationOptions: createIOSNavOptions(
                    s.MENU_LABEL_DASHBOARD,
                    null,
                    null,
                ),
            },
        }),
        navigationOptions: {
            tabBarLabel: s.MENU_LABEL_DASHBOARD,
        },
    },
    Containers: {
        screen: createStackNavigator({
            Containers: {
                screen: Containers,
                navigationOptions: () => createIOSNavOptions(
                    s.MENU_LABEL_CONTAINERS,
                    null,
                    null,
                ),
            },
        }),
        navigationOptions: {
            tabBarLabel: s.MENU_LABEL_CONTAINERS,
        },
    },
    Images: {
        screen: createStackNavigator({
            Images: {
                screen: Images,
                navigationOptions: ({ screenProps }) => createIOSNavOptions(
                    s.MENU_LABEL_IMAGES,
                    null,
                    <Feather
                        style={styles.rightAction}
                        name={i.DOWNLOAD}
                        size={30}
                        color={c.iosBlue}
                        onPress={() => screenProps.navigation.navigate(gC.IMAGE_ADD_ROUTE)}
                    />,
                ),
            },
        }),
        navigationOptions: {
            tabBarLabel: s.MENU_LABEL_IMAGES,
        },
    },
    Swarm: {
        screen: createStackNavigator({
            Swarm: {
                screen: Swarm,
                navigationOptions: createIOSNavOptions(
                    s.MENU_LABEL_SWARM,
                    null,
                    null,
                ),
            },
        }),
        navigationOptions: {
            tabBarLabel: s.MENU_LABEL_SWARM,
        },
    },
    SignOut: {
        screen: SignIn,
        navigationOptions: ({ screenProps }) => ({
            tabBarOnPress: () => {
                return Alert.alert(
                    s.SIGN_OUT_TITLE,
                    s.SIGN_OUT_TEXT,
                    [
                        {
                            text: s.BTN_OK,
                            onPress: () => {
                                screenProps.screenProps.navigate(gC.AUTH_ROUTE);
                                clearAsyncStorage();
                            },
                        },
                        { text: s.BTN_CANCEL },
                    ],
                );
            },
            tabBarLabel: s.MENU_LABEL_SIGN_OUT,
        }),
    },
}, {
    // nastaveni ikon pro obrazovky v navigaci
    navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ tintColor }) => {
            const { routeName } = navigation.state;
            let iconName;
            if (routeName === gC.DASHBOARD_ROUTE) {
                iconName = i.DASHBOARD;
            } else if (routeName === gC.CONTAINERS_ROUTE) {
                iconName = i.CONTAINERS;
            } else if (routeName === gC.IMAGES_ROUTE) {
                iconName = i.IMAGES;
            } else if (routeName === gC.SWARM_ROUTE) {
                iconName = i.SWARM;
            } else if (routeName === gC.SIGNOUT_ROUTE) {
                iconName = i.SIGNOUT;
            }

            return <Feather name={iconName} size={25} color={tintColor} />;
        },
    }),
    tabBarOptions: {
        activeTintColor: c.iosBlue,
        inactiveTintColor: c.darkerGrey120,
    },
    initialRouteName: gC.DASHBOARD_ROUTE,
});
